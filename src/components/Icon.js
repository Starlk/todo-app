import styled from "styled-components";
import device from "../responsive/breakPoint";

const Icon = styled.img.attrs(({icon})=>({src:icon}))`
cursor:pointer;
width:${({width})=>width + 'rem'};
`

const IconFlex = styled(Icon)`
align-self: flex-start;
`
const IconCheck = styled(Icon)`
vertical-align: middle;
visibility:${({completed})=> !completed ? "hidden" : null};
`
const IconDelete = styled(Icon)`
    visibility:hidden;
`
const Check = styled.div`
 padding:.1em;
 border-radius:100%;
 border-color:${({theme})=>theme.bg_color};
 border-style:solid;
 border-width:2px;
 display:inline-block;
 cursor:pointer;
 margin-left:1em;

 background-image:${({completed})=>(completed ? "linear-gradient(hsl(192, 100%, 67%), hsl(280, 87%, 65%))" : "none" ) };

 ${device.tablet`
   width:20px;
   height:50px
    `}

     ${device.mobile`
   width:20px;
   height:50px
    `}
`

export {Icon, IconFlex, IconCheck, Check, IconDelete};