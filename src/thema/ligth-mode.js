const ligthMode = {
    bg_color:"hsl(236, 33%, 92%);",
    bg_color_card:"hsl(0, 0%, 98%);",
    bg_text:'hsl(235, 19%, 35%);',
    text_completed:"hsl(236, 9%, 61%);",
    text:"hsl(233, 11%, 84%);",
    text_title:"hsl(0, 0%, 98%)",
    text_All:"hsl(220, 98%, 61%);",
    text_hover:"hsl(235, 19%, 35%)"
};

export default ligthMode