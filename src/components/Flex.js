import styled, { css } from "styled-components";
import device from '../responsive/breakPoint'
const Flex =  styled.div `
display:flex;
flex-direction:row;
justify-content:${({j_content})=>j_content};


${({grow})=> grow && css`
    flex-grow:${({grow})=>grow};
`}
`

const FlexMobile = styled(Flex)`
  ${device.mobile`
    flex-direction:column;
    justify-content:space-between;
  `}
`

export {Flex, FlexMobile}