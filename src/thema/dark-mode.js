const darkMode ={
    bg_color:"hsl(235, 21%, 11%);",
    bg_color_card:"hsl(235, 24%, 19%);",
    bg_text:'hsl(234, 39%, 85%);',
    text_completed:"hsl(233, 14%, 35%);",
    text:"hsl(237, 14%, 26%);",
    text_title:"hsl(0, 0%, 98%)",
    text_All:"hsl(220, 98%, 61%);",
    text_hover:"hsl(237, 14%, 26%)"
}

export default darkMode