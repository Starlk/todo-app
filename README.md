# Getting Started 

1. clone or download the repository to the folder of your choice
2. Open command line right in the project folder 
3. Enter the npm init command to download the project's dependencies 
4. Enter npm start to visualize the project in your browser 
5. Happy coding 

# screenshot

[Dark](./public/Capture/Dark.jpeg)
[Ligth](./public/Capture/Ligth.jpeg)

