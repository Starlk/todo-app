import React,{useState} from 'react'
import TitleStyled from './TitleStyled';
import {IconFlex} from './Icon';
import moon from '../assets/images/icon-moon.svg'
import sun from '../assets/images/icon-sun.svg'
import WrittenTasks from './WrittenTasks';
import {Flex} from './Flex';
import TasksTable from './TasksTable';
import Section from './Section'
export default function ToDo({ThemeLigth,HandleToggle}){
    const [tasks, setTasks] = useState([])
    const HanldeCompleted = (key) =>{
        setTasks(tasks.map((el)=>{
            if(el.id===key) {
                el.completed=!el.completed;
                return el
            }else return el
        }))
    }
    const HandleDelete = (key) =>{
        setTasks(tasks.filter(el=>(!(el.id === key))))
    }
    const HandleClear = () =>{
        setTasks(tasks.filter(el=>(!(el.completed === true))))
    }
    return( 
    <Section>
        <Flex j_content="space-between">
            <TitleStyled>TODO</TitleStyled>
            <IconFlex
            icon={ThemeLigth ? sun : moon}
            onClick={()=>HandleToggle(!ThemeLigth)}
            width={2}/>
        </Flex>
            <WrittenTasks setTasks={setTasks} tasks={tasks}/>
            <TasksTable 
            tasks={tasks} 
            HanldeCompleted={HanldeCompleted} 
            HandleDelete={HandleDelete}
            HandleClear={HandleClear}
            />
    </Section>
    );
}