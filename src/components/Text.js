import styled from "styled-components";

export default styled.span`
font-weight: 400;
font-size:.8rem;
color:${({theme,blue})=>blue?  theme.text_All : theme.text};
align-self:center;
cursor:pointer;

`