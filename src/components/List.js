import styled from "styled-components";

export default styled.ul`
    list-style: none;
    margin:0;
    margin-bottom:1em;
    padding:0;
`