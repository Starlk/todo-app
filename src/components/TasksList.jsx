import React from 'react'
import Item from './Item'
import { Check, IconCheck,IconDelete } from './Icon'
import check from '../assets/images/icon-check.svg'
import cross from '../assets/images/icon-cross.svg'
const TasksList = ({title,completed,HanldeCompleted,indx,HandleDelete}) =>{
return(
    <Item completed={completed}>
        <Check completed={completed} onClick={()=>HanldeCompleted(indx)}>
            <IconCheck icon={check} width={1.2} completed={completed}   />
        </Check>
        {title} 
        <IconDelete icon={cross} width={1.2} onClick={()=>HandleDelete(indx)}/> 
    </Item>
);
}

export default TasksList