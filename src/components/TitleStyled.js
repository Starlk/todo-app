import styled from "styled-components";
import device from "../responsive/breakPoint";

export default styled.h1`
    color:${({theme})=>theme.text_title};
    font-weight:700;
    font-size:2.6rem;
    letter-spacing:0.2em;
    display:inline;
    margin:1em

    ${device.mobile`
        font-size:1rem;
        margin-left:1.3em;
    `}
`