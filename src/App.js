import React,{useState,useEffect} from 'react'
import  {ThemeProvider} from 'styled-components'
import ToDo from './components/ToDo';
import darkMode from './thema/dark-mode';
import ligthMode from './thema/ligth-mode';




export default function App(){
  const [toggleTheme, setToggleTheme] = useState(false)

  useEffect(()=>{
     if(toggleTheme){
      document.body.classList.add("dark")
      document.body.classList.remove("ligth")
     }
     else {
      document.body.classList.add("ligth")
      document.body.classList.remove("dark")
     }
  },[toggleTheme])
  return (
    <>
    <ThemeProvider theme={toggleTheme ? darkMode :  ligthMode}>
        <ToDo 
          ThemeLigth={toggleTheme} 
          HandleToggle={setToggleTheme}/>
    </ThemeProvider>
    </>
  );

}