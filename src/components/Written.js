import styled from "styled-components";
import device from "../responsive/breakPoint";

export default styled.div`
    background-color:${({theme})=>theme.bg_color_card};
    border-radius:10px;
    transition-property: background-color;
    transition-duration: 2s;
    margin-bottom:2rem;

    ${device.mobile`
    display:flex;
    flex-direction:row;
    padding:1em;
`}

    ${device.tablet`
        display:flex;
        flex-direction:row;

    `}
`