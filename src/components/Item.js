import styled, { css } from "styled-components";

export default styled.li`
 padding:.5em;
 font-size:1rem;
 display: flex;
 justify-content:space-between;
 alifn-items:center;

 ${({completed})=> completed ? 
 css`
    color:${({theme})=>theme.text_completed} ;
    text-decoration:line-through;
 ` 
 : 
 css`
    color:${({theme})=>theme.bg_text};
 `}
 

 &:hover > img{
   visibility:visible;
   color:red;
 }
`