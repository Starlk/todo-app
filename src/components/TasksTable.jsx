import React from 'react'
import {FlexMobile} from './Flex';
import List from './List';
import Table from './Table';
import TasksList from './TasksList';
import Text from "./Text"
import { useState } from 'react';
export default function TasksTable({tasks,HanldeCompleted,HandleDelete,HandleClear}){
  const [viewTasks, setViewTasks] = useState(0)
  const HandleViewTasks =()=>{

    switch(viewTasks){
        case 0:
            return(
                tasks.map(({id,title,completed})=><TasksList key={id} indx={id} title={title}  completed={completed} HanldeCompleted={HanldeCompleted} HandleDelete={HandleDelete}/>
                )
            )
        case 1:
            const TasksFilterCompleted = tasks.filter(el=>!(el.completed))
            return(
                TasksFilterCompleted.map(({id,title,completed})=><TasksList key={id} indx={id} title={title}  completed={completed} HanldeCompleted={HanldeCompleted} HandleDelete={HandleDelete}/>
                )
            )
        case 2:
            const TasksFilterNotCompleted = tasks.filter(el=>el.completed)
            return(
                TasksFilterNotCompleted.map(({id,title,completed})=><TasksList key={id} indx={id} title={title}  completed={completed} HanldeCompleted={HanldeCompleted} HandleDelete={HandleDelete}/>
                )
            )
        default:
            setViewTasks(0) 
    }
  }
    return(
        <Table>
            <List>
                {
                    HandleViewTasks()
                }
            </List>
            <FlexMobile j_content="space-around">
                <Text>{tasks.length}  item</Text>
                <FlexMobile j_content="space-evenly" grow={2}>
                    <Text 
                    hover 
                    blue={viewTasks===0} 
                    onClick={()=>setViewTasks(0)}
                    >All</Text>
                    <Text 
                    hover 
                    blue={viewTasks===1}
                    onClick={()=>setViewTasks(1)}
                    >Active</Text>
                    <Text 
                    hover 
                    blue={viewTasks===2}
                    onClick={()=>setViewTasks(2)}
                    >Completed</Text>
                </FlexMobile>
                    <Text hover onClick={HandleClear}>Clear Completed</Text>
            </FlexMobile>
        </Table>
    );
}