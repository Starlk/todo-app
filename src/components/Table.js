import styled from "styled-components";

export default styled.div`
    background-color:${({theme})=>theme.bg_color_card};
    border-radius:5px;
    padding:1em;
`