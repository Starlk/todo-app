import styled from "styled-components";
import device from "../responsive/breakPoint";
export default styled.input.attrs(({placeholder})=>({type:"text", placeholder}))`
 background-color:transparent;
 color:${({theme})=>theme.bg_text};
 font-size:1.6rem;
 padding:0 1em;
 outiline:none;
 border-style:none;
 margin:.5em 0;

 &:focus{
    outline: none;
 }

 &::placeholder{
    color:${({theme})=>theme.bg_text};
 }



 ${device.mobile`
   font-size:1rem;
 `}
`