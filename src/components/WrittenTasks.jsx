import React from 'react'
import Written from './Written'
import Input from './Input'
import { IconCheck,Check } from './Icon'
import cross from '../assets/images/icon-check.svg'
import {generate as key} from "shortid"

const WrittenTasks = ({setTasks,tasks}) =>{

    // const HandlerTasks = (e) =>{
    // }
    
    const HandlerSubmit =(e)=>{
        e.preventDefault()
       setTasks([...tasks,
         {
             id:key(),
             title:e.target[0].value,
             completed:false,
             hora:new Date().toLocaleTimeString(),
             dia:new Date().toLocaleDateString()
         }
     ])
     e.target[0].value = ""
    }
    return(
    <form onSubmit={HandlerSubmit}>
        <Written>
            <Check>
                <IconCheck icon={cross} width={1.2} />
            </Check>
            <Input placeholder="Create a new todo..."  />
        </Written>
    </form>

    );
}

export default WrittenTasks
