import styled from "styled-components";
import device from "../responsive/breakPoint";
export default styled.section`

${device.mobile`
 max-width:90%;
 margin :0 auto;
`}

${device.tablet`
 max-width:90%;
`}

`