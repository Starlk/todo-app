const sizes = {
    mobile:"390px",
    tablet:"768px",
    laptop:"1024px",
    desktop:"1400px"
}

const device = {
    mobile: (style) =>{
        return `@media screen and (max-width:${sizes.mobile}){
            ${style}
        }`
    },
    tablet: (style) =>{
        return `@media screen and (max-width:${sizes.tablet}){
            ${style}
        }`
    }
}


export default device